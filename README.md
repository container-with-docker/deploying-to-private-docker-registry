# Deploying to private Docker Registry

## Pushing to AWS ECR container existing_repo

The workflow:
- create private repo at AWS ECR
- Use IAM service to assign roles and permission for a user in AWS  
  assign policies for this user to accept incoming images, edit and delete them.
  create assess key and secret keys that one will use to authenticate with aws inorder 
  to push the image locally to ECR.
- AWS will provide a guide on how an image should be push to that specific repo.


```
Technology Used
Docker
Nodejs
AWS ECR

```

```
cd existing_repo
git remote add origin https://gitlab.com/container-with-docker/deploying-to-private-docker-registry.git
git branch -M main
git push -uf origin main
```

